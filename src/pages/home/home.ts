import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { NFC, Ndef } from '@ionic-native/nfc';
import { Subscription } from 'rxjs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  readingTag:   boolean   = false;
  writingTag:   boolean   = false;
  isWriting:    boolean   = false;
  ndefMsg:      string    = '';
  subscriptions: Array<Subscription> = new Array<Subscription>();

  constructor(public navCtrl: NavController, 
    private toast: ToastController,
    public nfc: NFC,
    public ndef: Ndef,) {


    this.subscriptions.push(this.nfc.addNdefListener()
    .subscribe(data => {
      if (this.readingTag) {
        let payload = data.tag.ndefMessage[0].payload;
        let tagContent = this.nfc.bytesToString(payload).substring(3);
        this.readingTag = false;
        console.log("tag data", tagContent);
        console.log("msg data", this.ndefMsg);
        console.log(tagContent == this.ndefMsg);
        
        this.ndefMsg = tagContent;
        let toast2 = this.toast.create({
          message: 'Content readed!',
          showCloseButton: true,
          position: 'down',
          closeButtonText: 'Ok'
        });
        toast2.present();
      } 
      else if (this.writingTag) {
        if (!this.isWriting) {
          this.isWriting = true;
          let value = this.ndef.textRecord(this.ndefMsg);
          this.nfc.write([value])
            .then(() => {
              this.writingTag = false;
              this.isWriting = false;
              console.log("written");
              this.readingTag = true;
              let toast1 = this.toast.create({
                message: 'Content written succesfully!',
                showCloseButton: true,
                position: 'down',
                closeButtonText: 'Ok'
              });
              toast1.present();
            

            })
            .catch(err => {
              this.writingTag = false;
              this.isWriting = false;
              console.log("Error writting")
              console.log(err);
            });
        }
      }
    },
    err => {
    
    })
 );
  }
  readTag() {
    this.readingTag = true;
  }
  writeTag(){
    this.writingTag = true;
  }
}
